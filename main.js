if(require.main === module) {
	console.error('This package is not directly runnable.\nPlease refer to the README.')
	process.exit(1)
}


// native modules
const util = require('util')


const storage = new WeakMap

class Thanos {
	static DEFAULT_RNG = Math.random

	constructor(options) {
		options = {...options}
		storage.set(this, options)
	}

	/**
	 * Performs a snap and pipes your inputs through the `balance` function
	 * @param {string|Buffer|number|Array} inputs The value(s) you'd like to bring balance to
	 * @return {string|Buffer|number|Array|*} The balanced version of your input(s)
	 */
	snap(...inputs) {
		if(!inputs.length) return undefined

		const rng = this.rng
		inputs = inputs.map(input => {
			try {
				return balance(input, rng)
			} catch(err) {
				throw new SnapError(err.message)
			}
		})

		if(inputs.length === 1) return inputs[0]
		return inputs
	}

	/**
	 * Returns the RNG used internally
	 * @return {function|GeneratorFunction}
	 */
	get rng() {
		return storage.get(this).rng || this.constructor.DEFAULT_RNG
	}

	/**
	 * Sets a custom RNG to be used with the gauntlet snap
	 * @param {function|GeneratorFunction} rng A function (or generator) returning a random value in the range 0–1 (inclusive of 0, but not 1)
	 */
	set rng(rng) {
		if(typeof rng !== 'function') throw new TypeError('Given value is not a function.')

		if(rng.constructor.name === 'GeneratorFunction') {
			const generator = rng()
			storage.get(this).rng = () => {
				const next = generator.next()
				if(next.done) {
					if(next.value) return next.value
					throw new RangeError('RNG has stopped yielding values.')
				}
				return next.value
			}
		} else {
			storage.get(this).rng = rng
		}
	}
}

/**
 * Erases half of the contents of the array passed in, based on the logic of `halveNum`
 * @param {Array} array The array that shall be halved
 * @param {function?} rng The random number generator to use
 * @return {Array} A new array with half the contents
 */
const halveArr = (array, rng = Thanos.DEFAULT_RNG) => {
	if(!(typeof array === 'object' && Array.isArray(array))) throw new TypeError('Given value is not an array.')

	let count = halveNum(array.length, rng)

	let result = [...array]
	for(let i = 0; i < count; i++) {
		const r = Math.floor(rng() * result.length)
		result.splice(r, 1)
	}

	return result
}

/**
 * Halves the number passed in. Randomly decides to either round up or down in case there's fractions
 * @param {number} number The number that shall be halved
 * @param {function?} rng The random number generator to use
 * @return {number} Half of the rounded number plus its original fraction
 */
const halveNum = (number, rng = Thanos.DEFAULT_RNG) => {
	if(typeof number !== 'number') throw new TypeError('Given value is not a number.')

	number = number / 2

	if(!Number.isInteger(number))
		number = !!Math.round(rng())
			? Math.floor(number)
			: Math.ceil(number)

	return number
}

/**
 * Erases half of whatever* you pass in to make it perfectly balanced ... as all things should be
 * @param {string|Buffer|number|Array} input The thing you want to erase half of
 * @param {function?} rng The random number generator to use
 * @return {string|Buffer|number|Array|*} Either the balanced version of the input or, if unsupported, the unprocessed input
 */
const balance = (input, rng = Thanos.DEFAULT_RNG) => {
	if(!input) return input

	if(typeof input === 'string' || input instanceof Buffer) {
		const array = input.toString().split('')
		const result = halveArr(array, rng).join('')

		if(input instanceof Buffer) return Buffer.from(result)
		return result
	} else if(typeof input === 'number') {
		if(input < 0) throw new Error(`Can't erase half of less than nothing: ${input}`)
		return halveNum(Math.round(input), rng) + (input - Math.round(input))
	} else if(typeof input === 'object' && Array.isArray(input)) {
		return halveArr(input)
	}

	return input
}

/**
 * Snapping doesn't always work correctly ... right?
 */
class SnapError extends Error {
	constructor(message) {
		super(message)
		this.name = this.constructor.name

		Error.captureStackTrace(this, SnapError)
	}
}


module.exports = {
	/** @deprecated Instantiate Thanos yourself using the exported `Thanos` class */
	get thanos() {
		return util.deprecate(() => new Thanos(), 'Instantiate Thanos yourself using the exported `Thanos` class')
	},

	Thanos,
	SnapError
}
