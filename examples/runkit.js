const { Thanos } = require('thaj')
const thanos = new Thanos()

console.log(thanos.snap('Stan Lee')) // 'tnee'
console.log(thanos.snap(Buffer.from('New York'))) // <Buffer 77 20 6f 6b> => 'w ok'

console.log(thanos.snap(42)) // 21
console.log(thanos.snap(21)) // 10 or 11
console.log(thanos.snap(Math.PI)) // 1.1415926535897931 or 2.141592653589793

console.log(thanos.snap([2, 5])) // [2] or [5]

// you can also pass in multiple values:
console.log(thanos.snap('I am inevitable', 3000, [2, 5]))
