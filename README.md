# thaj [![npm][npm-badge]][npm-link] <img src="https://gitlab.com/metaa/thaj/raw/master/doc/logo.png" align="right">
> as in "[Thanos](https://en.wikipedia.org/wiki/Thanos) JS", just (the exceptionally perfect) half of it

This package allows you to use the powers of evil people with
[mythical golden gauntlets](https://en.wikipedia.org/wiki/The_Infinity_Gauntlet) to erase half of pretty much whatever*
you want.

<sub>\* *as long as it is divisible* (it keeps fractions intact)</sub>

## Installation
```bash
npm i thaj
```

## Usage
### In scripts
**Note:** Results may vary.

```js
const { Thanos } = require('thaj')
const thanos = new Thanos()

thanos.snap('Stan Lee') // 'tnee'
thanos.snap(Buffer.from('New York')) // <Buffer 77 20 6f 6b> => 'w ok'

thanos.snap(42) // 21
thanos.snap(21) // 10 or 11
thanos.snap(Math.PI) // 1.1415926535897931 or 2.141592653589793

thanos.snap([2, 5]) // [2] or [5]

// you can also pass in multiple values:
thanos.snap('I am inevitable', 3000, [2, 5])
```

You can also optionally apply a custom [RNG](https://en.wikipedia.org/wiki/Random_number_generation) for your own grain
of randomness ([`GeneratorFunction`](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/GeneratorFunction)s
are also supported)!

Just make sure that its values range from 0 to 1 (excluding 1) like in [`Math.random`](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Math/random).

```js
// change the default for all instances
Thanos.DEFAULT_RNG = myCustomRNG

// change it for once instance
thanos.rng = myCustomRNG
```

### On the command line
The CLI variant `thanos` essentially works like the [unix](https://en.wikipedia.org/wiki/Unix) command
[`cat`](https://en.wikipedia.org/wiki/Cat_(Unix)), just half as good.

```bash
$ echo "Stark Tower" > /tmp/starktower.txt
$ thanos /tmp/starktower.txt
takToe

$ thanos -
Captain America
panmerca
```

## License
Click to read: [![NPM](https://img.shields.io/npm/l/thaj.svg?color=993366)](https://gitlab.com/metaa/thaj/blob/master/LICENSE)

[npm-badge]: https://img.shields.io/npm/v/thaj.svg
[npm-link]: https://www.npmjs.com/package/thaj
