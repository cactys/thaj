#!/usr/bin/env node
const BIN_NAME = 'thaj'

/**
 * This pretty much works like the linux command "cat"
 */
/**/

// core modules
const fs = require('fs')

// local files
const { Thanos } = require('../')
const thanos = new Thanos

const pkg = require('../package.json')


const params = process.argv.slice(2).map(param => param.trim())

// Very basic cli argument handling, but it's okay for now:
switch(params[0]) {
	case '-h':
	case '--help': {
		console.log([
			`Usage: ${BIN_NAME} [OPTION]... [FILE]...`,
			'Concatenate snapped FILE(s) to standard output.',
			'',
			'With No FILE, or when FILE is -, read standard input.',
			'',
			'  -c            collect input and only snap it when it ends',
			'  -h, --help    display this help and exit',
			'  --version     output version information and exit',
			'',
			'Examples:',
			`  ${BIN_NAME} Avengers    Outputs the snapped version of file "Avengers"`,
			`  ${BIN_NAME}             Copy snapped standard input to standard output`,
			'',
			`Online reference: ${pkg.homepage}`
		].join('\n'))
		return
	}

	case '--version': {
		console.log(`${pkg.name} (NPM package) ${pkg.version}`)
		return
	}

	case '-':
	case '-c':
	case undefined: {
		const stdin = process.openStdin()

		let data = ''

		stdin.on('data', chunk => {
			if(params[0] === '-c') data += chunk
			else console.log(thanos.snap(chunk.toString().trim()))
		})

		stdin.on('end', () => {
			if(params[0] === '-c') console.log(thanos.snap(data.trim()))
		})
		break
	}

	default: {
		if(params[0].startsWith('-')) {
			console.error(`${BIN_NAME}: invalid option -- '${params[0].replace(/^-([^-])/, '$1')}'`)
			return process.exit(1)
		}

		params.forEach(file => {
			fs.stat(file, (err, stat) => {
				if(err) {
					switch(err.code) {
						case 'ENOENT': {
							console.error(`${BIN_NAME}: ${err.path}: No such file or directory`)
							break
						}
						case 'EACCES': {
							console.error(`${BIN_NAME}: ${err.path}: Permission denied`)
							break
						}
						default: {
							console.error(`${BIN_NAME}: ${err.path}: General error: ${err.code}`)
							break
						}
					}
					return process.exit(1)
				}

				if(stat.isDirectory()) {
					console.error(`${BIN_NAME}: ${file}: Is a directory`)
					return process.exit(1)
				}

				////////

				fs.readFile(file, (err, content) => {
					content = content.toString()
					console.log(thanos.snap(content))
				})
			})
		})
	}
}

